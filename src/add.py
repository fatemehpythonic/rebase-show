

def addition(num1:int, num2: int):
    """This function aims to add 2 int number together. 

    Args:
        num1: an integer number number
        num2: an integer number
    Returns:
        addition of num1 and num2 integers
    """
    if type(num1)==str or type(num2)==str:
        print("can't!")

    return num1+num2

def subtraction(num1:int, num2: int):
    """This function aims to sub 2 int number together. 

    Args:
        num1: a number
        num2: a number
    Returns:
        subtraction of num1 and num2 integers
    """

    return num1-num2